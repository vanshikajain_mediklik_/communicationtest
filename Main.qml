import QtQuick 2.15
import QtQuick.Controls 2.15
import QtSerialPort 5.15

ApplicationWindow {
    visible: true
    width: 640
    height: 480

    SerialPort {
        id: serialPort
        portName: "/dev/ttyUSB0"  // Replace with your serial port name
        baudRate: 9600
        dataBits: SerialPort.Data8
        parity: SerialPort.NoParity
        stopBits: SerialPort.OneStop
        openMode: SerialPort.ReadWrite

        onOpened: {
            console.log("Serial port opened.")
        }

        onErrorOccurred: {
            console.error("Error occurred:", errorString)
        }

        onReadyRead: {
            var data = readAll()
            console.log("Received data:", data)
            // Handle received data here
        }
    }
}
